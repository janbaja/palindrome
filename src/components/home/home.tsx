import React, { Props } from 'react';
import { APP_ROUTES } from '../../models/RoutingPaths';
import LabelRequired from '../common/LabelRequired';
import { withRouter, RouteComponentProps } from 'react-router';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

interface HomeState {
    inputWordLevelOne: string;
    inputWordLevelTwo: string;
    inputWordLevelThree: string;
    answerLevelOne: any;
    answerLevelTwo: any;
    answerLevelThree: any;
    errorMessageOne: any;
    errorMessageTwo: any;
    errorMessageThree: any;
    levelOne: boolean;
    levelTwo: boolean;
    levelThree: boolean;
}

interface DispatchHomeProps {
}

interface HomeProps extends DispatchHomeProps, RouteComponentProps {

}

class Home extends React.Component<HomeProps, HomeState> {

    constructor(props: any) {
        super(props);

        this.state = {
            levelOne: false,
            levelTwo: false,
            levelThree: false,
            inputWordLevelOne: '',
            inputWordLevelTwo: '',
            inputWordLevelThree: '',
            answerLevelOne: '',
            answerLevelTwo: '',
            answerLevelThree: '',
            errorMessageOne: '',
            errorMessageTwo: '',
            errorMessageThree: '',
        }
    }

    componentDidMount() {
		this.setState({ levelOne: true });
	}

    palindromeChecker(str: any) {
        var rev = str.split("").reverse().join("");
        return str == rev;
    }

    longestPalindrome(str: any) {
        var max_length = 0,
        maxp = '';
        
        for(var i=0; i < str.length; i++) 
        {
            var subs = str.substr(i, str.length);
            
            for(var j=subs.length; j>=0; j--) 
            {
            var sub_subs_str = subs.substr(0, j);
            if (sub_subs_str.length <= 1)
            continue;
            
            if (this.palindromeChecker(sub_subs_str))
            {
                if (sub_subs_str.length > max_length) 
                    {
                        max_length = sub_subs_str.length;
                        maxp = sub_subs_str;
                    }
                }
            }
        }
    
        return maxp;
    }

    countPalindromesInString(s: any) {
        let count = 0;
        let subString;
    
        for (let i = 1; i < s.length; i++) {
          for(let j = 0; j < s.length - i; j++) {
            subString = s.substring(j, j+i+1);
            if(subString === subString.split('').reverse().join('')) {
                if(subString.length > 2) {
                    count += 1;
                }
            }
          }
        }

        return count;
    }

    checkPalindrome() {
        var palindromeWord = this.state.inputWordLevelOne;
        if(palindromeWord.replace(/\s/g, "") == "") {
            this.setState({ errorMessageOne: "This is a required field." });
        } else {
            this.setState({ errorMessageOne: '', answerLevelOne: this.palindromeChecker(palindromeWord) ? 'true' : 'false' });
        }
    }

    checkLongestPalindrome() {
        var palindromeWord = this.state.inputWordLevelTwo;
        var pass = true;
        if (palindromeWord.replace(/\s/g, "") == "") {
            this.setState({ errorMessageTwo: "This is a required field." });
            pass = false;
        }

        if (pass)
            this.setState({ errorMessageTwo: '', answerLevelTwo: this.longestPalindrome(palindromeWord) });
        
    }

    checkCountOfPalindrome() {
        var palindromeWord = this.state.inputWordLevelThree;
        var pass = true;
        if (palindromeWord.replace(/\s/g, "") == "") {
            this.setState({ errorMessageThree: "This is a required field." });
            pass = false;
        }

        if (pass)
            this.setState({ errorMessageThree: '', answerLevelThree: this.countPalindromesInString(palindromeWord) });
    }

    render() {
        return (
            <div style={{ scrollBehavior: 'smooth' }}>

                <section id="z-home-page" className="z-homepage">
                    <div className="z-homepage-container">
                    
                    <nav className="navbar sticky-top navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="#">REACTJS & BOOTSTRAP</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                            </ul>
                            <form className="form-inline my-2 my-lg-0">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>

                    </nav>
                    
                    </div>
                </section>

                <div className="container">
                    <div className="container-fluid m-5">
                        <div className="row align-items-start">
                            <div className="col">
                                <ul className="nav nav-tabs">
                                    <li className="nav-item">
                                        <a className={this.state.levelOne ? "nav-link active" : "nav-link pointer"}  onClick={() => this.setState({ levelOne: true, levelTwo: false, levelThree: false }) }>Challenge Level 1</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className={this.state.levelTwo ? "nav-link active" : "nav-link pointer"}  onClick={() => this.setState({ levelOne: false, levelTwo: true, levelThree: false }) }>Challenge Level 2</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className={this.state.levelThree ? "nav-link active" : "nav-link pointer"}  onClick={() => this.setState({ levelOne: false, levelTwo: false, levelThree: true }) }>Challenge Level 3</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        
                        {this.state.levelOne && <div className="row">
                            <div className="col">
                                
                                <div className="p-3 text-left border-right border-bottom border-left">

                                    <div className="mt-5">
                                        <div>
                                            Write a <span className="font-weight-bold">function</span> that takes in a non-empty string and that returns a boolean representing whether the string is a palindrome.
                                        </div>
                                        <div className="mt-3">
                                            <div className="form-group">
                                                <div className="">
                                                    <LabelRequired labelText={'Input A Word'} htmlFor="text-word" required={true} />
                                                    <input
                                                        id="text-word"
                                                        type="text"
                                                        onChange={(e) => {this.setState({inputWordLevelOne: e.target.value})} }
                                                        className="form-control"
                                                        placeholder=""
                                                    />
                                                    {this.state.errorMessageOne && <div><div className="alert alert-danger" role="alert">{this.state.errorMessageOne}</div></div>}
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button
                                                type="button"
                                                className="btn btn-primary"
                                                onClick={() => this.checkPalindrome()}
                                            >
                                                Verify
                                            </button>
                                        </div>

                                        <div className="card mt-5">
                                            <h5 className="card-header">Palindrome?</h5>
                                            <div className="card-body">
                                                <h5 className="card-title">{this.state.answerLevelOne}</h5>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>}


                        {this.state.levelTwo && <div className="row">
                            <div className="col">
                                
                                <div className="p-3 text-left border-right border-bottom border-left">

                                    <div className="mt-5">
                                        <div>
                                            Now write a <span className="font-weight-bold">function</span> that, given a string, returns its longest palindromic substring. <br/>
                                            You can assume that there will only be one longest palindromic substring. 
                                        </div>
                                        <div className="mt-3">
                                            <div className="form-group">
                                                <div className="">
                                                    <LabelRequired labelText={'Input A Word'} htmlFor="text-word" required={true} />
                                                    <input
                                                        id="text-word"
                                                        type="text"
                                                        onChange={(e) => {this.setState({inputWordLevelTwo: e.target.value})} }
                                                        className="form-control"
                                                        placeholder=""
                                                    />
                                                    {this.state.errorMessageTwo && <div><div className="alert alert-danger" role="alert">{this.state.errorMessageTwo}</div></div>}
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button
                                                type="button"
                                                className="btn btn-primary"
                                                onClick={() => this.checkLongestPalindrome()}
                                            >
                                                Verify
                                            </button>
                                        </div>

                                        <div className="card mt-5">
                                            <h5 className="card-header">Longest Palindromic Word</h5>
                                            <div className="card-body">
                                                <h5 className="card-title">{this.state.answerLevelTwo}</h5>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>}

                        {this.state.levelThree && <div className="row">
                            <div className="col">
                                
                                <div className="p-3 text-left border-right border-bottom border-left">

                                    <div className="mt-5">
                                        <div>
                                            Now write a <span className="font-weight-bold">function</span> that returns the minimum number of cuts needed to perform on the string such that each remaining substring is a palindrome.
                                        </div>
                                        <div className="mt-3">
                                            <div className="form-group">
                                                <div className="">
                                                    <LabelRequired labelText={'Input A Word'} htmlFor="text-word" required={true} />
                                                    <input
                                                        id="text-word"
                                                        type="text"
                                                        onChange={(e) => {this.setState({inputWordLevelThree: e.target.value})} }
                                                        className="form-control"
                                                        placeholder=""
                                                    />
                                                    {this.state.errorMessageThree && <div><div className="alert alert-danger" role="alert">{this.state.errorMessageThree}</div></div>}
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <button
                                                type="button"
                                                className="btn btn-primary"
                                                onClick={() => this.checkCountOfPalindrome()}
                                            >
                                                Verify
                                            </button>
                                        </div>

                                        <div className="card mt-5">
                                            <h5 className="card-header">Numbers of existing Palindrome</h5>
                                            <div className="card-body">
                                                <h5 className="card-title">{this.state.answerLevelThree}</h5>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>}


                    </div>
                </div>

                <footer className="ci-footer">

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-2">
                            </div>
                            <div className="col-md-4">
                                
                            </div>
                            <div className="col-md-4">
                               
                            </div>
                            <div className="col-md-2">
                                
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        )
    }



}



export default Home;
