import React, { Component, Suspense } from 'react';
import { Routing, PrivateRouting } from "./utils/Routing";
import { Switch, Redirect } from 'react-router';
import "./styles/main.scss";
import './App.css';

import { APP_ROUTES } from "./models/RoutingPaths";

const Home = React.lazy(() => import('./components/home/home'));

class App extends Component {


	render() {
		return (
			<div className="App">
				
				<div className="main-wrapper">
				
					<Suspense fallback="">

						<div className="main-content">
						<Switch>
							<Routing path={APP_ROUTES.HOME} exact={true} component={Home} props={null} />
							<Redirect to={APP_ROUTES.HOME} />
						</Switch>
						</div>
					</Suspense>

				</div>
			</div>

		);
	}
}


export default App;
